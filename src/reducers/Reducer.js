import { Action, combineReducers } from 'redux';
//import HomeReducer from '../containers/Home/HomeReducer';

const reducers = combineReducers({
  //homeReducer: HomeReducer
});

const reducer = (state, action) => {
  return reducers(state, action);
};

export default reducer;
