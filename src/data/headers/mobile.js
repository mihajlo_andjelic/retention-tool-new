import React from 'react';
import SelectFilter from '../../components/Filter/SelectFilter';

const header = [
  {
    Header: () => null,
    id: 'expander',
    columns: [
      // {
      //   Header: 'Id',
      //   accessor: 'id'
      // },
      {
        Header: 'Carrier',
        accessor: 'carrier'
      },
      {
        Header: 'Product',
        accessor: 'product',
        filter: 'fuzzyText'
      },
      {
        Header: 'Internet',
        accessor: 'internet',
        filter: 'fuzzyText'
      },
      {
        Header: 'Calls',
        accessor: 'calls',
        Filter: SelectFilter,
        filter: 'include'
      },
      {
        Header: 'SMS',
        accessor: 'sms'
      },
      {
        Header: 'Device',
        accessor: 'device'
      },
      {
        Header: 'Roaming',
        accessor: 'roaming'
      },
      // {
      //   Header: 'Activation total',
      //   accessor: 'activation_total',
      //   filter: 'between'
      // },
      {
        Header: 'Activation',
        accessor: 'activation',
        filter: 'between'
      },
      // {
      //   Header: 'Street price low',
      //   accessor: 'street_price_low',
      //   filter: 'between'
      // },
      // {
      //   Header: 'Street price high',
      //   accessor: 'street_price_high',
      //   filter: 'between'
      // },
      {
        Header: 'Street price',
        accessor: 'street_price',
        filter: 'between'
      },
      // {
      //   Header: 'Promo price low',
      //   accessor: 'promo_price_low',
      //   filter: 'between'
      // },
      // {
      //   Header: 'Promo price high',
      //   accessor: 'promo_price_high',
      //   filter: 'between'
      // },
      {
        Header: 'Promo price',
        accessor: 'promo_price',
        filter: 'fuzzyText'
      },

      // {
      //   Header: 'Compare',
      //   id: 'compare_id',
      //   accessor: 'compare',
      //   isVisible: false
      // },
      {
        // Make an expander cell
        Header: () => null, // No header
        id: 'expander', // It needs an ID
        Cell: ({ row }) => {

          return (
            // Use Cell to render an expander for each row.
            // We can use the getToggleRowExpandedProps prop-getter
            // to build the expander.
            <>
              <span {...row.getToggleRowExpandedProps()}>
                {row.isExpanded ? '👇' : 'expand'}
              </span>
              <br />
              <span>
                <a href={row.original.details}>Details</a>
              </span>
            </>
          )
        },
        // We can override the cell renderer with a SubCell to be used with an expanded row
        SubCell: () => null // No expander on an expanded row
      },
      // ,
      // {
      //   Header: 'Details',
      //   accessor: 'details'
      // }
    ]
  }
]

export default header;