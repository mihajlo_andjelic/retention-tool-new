function fetchData(url = 'https://api.mocki.io/v1/9108e787', callbackFunction) {

  fetch(url)
    .then(response => response.json())
    .then(data => {
      console.log('columns', data);
      callbackFunction(data);

    });
}



export default fetchData;