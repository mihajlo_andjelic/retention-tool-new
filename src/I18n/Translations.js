const en = [];
const it = [];
const de = [];
const fr = [];

const I18N = {
  en: en,
  it: it,
  de: de,
  fr: fr
}

export default I18N;