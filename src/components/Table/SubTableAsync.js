import React from 'react';
import SubTable from './SubTable';

function SubTableAsync({ row, rowProps, visibleColumns }) {
  const [loading, setLoading] = React.useState(true);
  const [data, setData] = React.useState([]);

  React.useEffect(() => {

    //https://jsonplaceholder.typicode.com/users

    const a = [{
      carrier: 'kako sad',
      name: 'kako sad',
      down_up: 'kako sad',
      calls: 'kako sad',
      tv: 'kako sad',
      hw: 'kako sad',
      street_price: 'kako sad',
      promo_price: 'kako sad',
      technology: 'kako sad',
      compare: 'kako sad',
      details: 'kako sads'
    },
    {
      carrier: 'kako sad1',
      name: 'kako sad1',
      down_up: 'kako sad1',
      calls: 'kako sad1',
      tv: 'kako sad1',
      hw: 'kako sad1',
      street_price: 'kako sad1',
      promo_price: 'kako sad1',
      technology: 'kako sad1',
      compare: 'kako sad1',
      details: 'kako sads1'
    }];

    //https://jsonplaceholder.typicode.com/users
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(data => {
        const dataFetched = data.slice(0, Math.ceil(Math.random() * 10)  ).map( dat => {
          return {
            carrier: dat.address.city,
            name: dat.name,
            down_up: dat.address.city,
            calls: dat.phone,
            tv: dat.phone,
            hw: dat.website,
            street_price: dat.company.name,
            promo_price: '50$',
            technology: dat.address.zipcode,
            compare: dat.address.phone,
            details: dat.address.street
          };
         })
        setData( dataFetched );
        setLoading(false);
        console.log(data)
      });

    const timer = setTimeout(() => {
      // setData( a );
      // setLoading(false);
    }, 500);

    return () => {
      clearTimeout(timer);
    };

  }, []);

  return (
    <SubTable
      row={row}
      rowProps={rowProps}
      visibleColumns={visibleColumns}
      data={data}
      loading={loading}
    />
  );
}



export default SubTableAsync ;