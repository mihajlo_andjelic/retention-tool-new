import React from 'react';
import {useFilters, useGlobalFilter, useTable, useSortBy, usePagination, useExpanded} from 'react-table';
import FilterWrapper from '../Filter/FilterWrapper';
import Pagination from './Pagination';

export default function Table({
  columns,
  data,
  renderRowSubComponent,
  defaultColumn = null,
  filterTypes = null
}) {

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,

    //rows,
    page,
     // The rest of these things are super handy, too ;)
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    visibleColumns,
    state: { pageIndex, pageSize, expanded, globalFilter },
    preGlobalFilteredRows,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
      defaultColumn,
      filterTypes,
    },

    useFilters,
    useGlobalFilter,
    useSortBy,
    useExpanded,
    usePagination,

  );


  return (
    <>
      <FilterWrapper
        headerGroups={headerGroups}
        preGlobalFilteredRows={preGlobalFilteredRows}
        globalFilter={globalFilter}
        setGlobalFilter={setGlobalFilter}

      />
    <table className="s20-table" {...getTableProps()}>
      <thead className="s20-table__header">
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => {

              //console.log('wtf', column);

              return (

                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                  {column.render('Header')}
                  {/* Add a sort direction indicator */}
                  <span>
                    {column.isSorted
                      ? column.isSortedDesc
                        ? ' 🔽'
                        : ' 🔼'
                      : ' 🔽🔼'}
                  </span>
                </th>
              )
            })}
          </tr>
        ))}
      </thead>
      <tbody className="s20-table__body" {...getTableBodyProps()}>
        {page.map((row, i) => {
          prepareRow(row);
          const rowProps = row.getRowProps();
          return (
             <React.Fragment key={rowProps.key}>
              <tr {...rowProps}>
              {row.cells.map(cell => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
              })}
              </tr>
              {/* We could pass anything into this */}
              {row.isExpanded &&
                renderRowSubComponent({ row, rowProps, visibleColumns })}
              </React.Fragment>
          )
        })}
      </tbody>
    </table>
      <Pagination
        pageIndex={pageIndex}
        pageSize={pageSize}
        canPreviousPage = {canPreviousPage}
        canNextPage = {canNextPage}
        pageOptions = {pageOptions}
        pageCount = {pageCount}
        gotoPage = {gotoPage}
        nextPage = {nextPage}
        previousPage = {previousPage}
        setPageSize = {setPageSize}
      />


    </>
  )
}
