import React, { useState, useEffect, useCallback, useMemo } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Table from './Table/Table';
import SubRowAsync from './Table/SubTableAsync';

import "regenerator-runtime/runtime.js";

import headerWin from '../data/headers/win';
import headerMobile from '../data/headers/mobile';
import dummy_mobile from '../data/dummy/dummy_mobile';
import dummy_win from '../data/dummy/dummy_win';
import fetchDataCallback from '../data/fetch/fetching';

import DefaultFilter from './Filter/DefaultColumnFilter';
import FuzzyFilter from './Filter/FuzzyFilter';

import '../styles/style.css';


function App() {

  const [tabIndex, setTabIndex] = useState(0);
  const [tableColumnsWin, setTableColumnsWin] = useState(headerWin);
  const [tableColumnsMobile, setTableColumnsMobile] = useState(headerMobile);

  //api.mocki.io/v1/9108e787
  useEffect(() => {
    fetchDataCallback('https://api.mocki.io/v1/9108e787', setTableColumnsWin);
  }, [])

  useEffect(() => {
    fetchDataCallback('https://api.mocki.io/v1/9108e787', setTableColumnsMobile);
  }, [])

  const columnsWin = useMemo(() => headerWin, []);
  const columnsMobile = useMemo(() => headerMobile, []);

  const dataMobile = useMemo(() => dummy_mobile, []);
  const dataWin = useMemo(() => dummy_win, []);


  // Create a function that will render our row sub components
  const renderRowSubComponent = useCallback(
    ({ row, rowProps, visibleColumns }) => (
      <SubRowAsync
        row={row}
        rowProps={rowProps}
        visibleColumns={visibleColumns}
      />
    ),
    []
  );


  const filterTypes = useMemo(
    () => ({
      // Add a new fuzzyTextFilterFn filter type.
      fuzzyText: FuzzyFilter,
      // Or, override the default text filter to use
      // "startWith"
      text: (rows, id, filterValue) => {

        return rows.filter(row => {
          const rowValue = row.values[id]
          return rowValue !== undefined
            ? String(rowValue)
              .toLowerCase()
              .startsWith(String(filterValue).toLowerCase())
            : true
        })
      },
    }),
    []
  )

  const defaultColumn = useMemo(
    () => ({
      // Let's set up our default Filter UI
      Filter: DefaultFilter,
    }),
    []
  )

  return (
    <div className="App">

      <Tabs
        className="s20-tabs"
        selectedIndex={tabIndex}
        onSelect={index => { console.log(index); setTabIndex(index) }}
      >
        <TabList className="s20-tabs__selectors">
          <Tab className="s20-tabs__selectors-tab">WIN</Tab>
          <Tab className="s20-tabs__selectors-tab">MOBILE</Tab>
        </TabList>

        <TabPanel className="s20-tabs__tab">
          <Table
            columns={columnsWin}
            data={dataWin}
            renderRowSubComponent={renderRowSubComponent}
            defaultColumn={defaultColumn}
            filterTypes={filterTypes}
          />
        </TabPanel>
        <TabPanel className="s20-tabs__tab">

          <Table
            columns={columnsMobile}
            data={dataMobile}
            renderRowSubComponent={renderRowSubComponent}
            defaultColumn={defaultColumn}
            filterTypes={filterTypes}
          />

        </TabPanel>

      </Tabs>



    </div>
  );
}

export default App;
