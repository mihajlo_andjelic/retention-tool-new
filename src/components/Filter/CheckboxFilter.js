import React from 'react';

// This is a custom filter UI for selecting
// a unique option from a list
function CheckBoxFitler({
  column: { filterValue, setFilter, preFilteredRows, id },
  nonValue
}) {

  const [checked, setChecked] = React.useState(false);
  //or filter here
  const options = React.useMemo(() => {
    const options = new Set()

    preFilteredRows.forEach(row => {
      options.add(row.values[id])
    });


    return Array.from(options.values())
  }, [id, preFilteredRows])

  // Render a multi-select box
  return (
    <>
      <input
        type="checkbox"
        checked={checked}
        onClick={e => {
          setChecked(!checked);
          setFilter(checked ? false : nonValue) //value for filtering
        }}
        onChange={e => {

          // console.log('checkced')
          // setFilter(e.target.value !== nonValue || undefined)
        }}
      />

    </>
  )
}


export default CheckBoxFitler;