import React from 'react';
import GlobalFilter from './GlobalFilter';
import SelectFilter from './SelectFilter';
import NumberFilter from './NumberFilter';
import CheckboxFilter from './CheckboxFilter';

export default function FilterWrapper({
  headerGroups,
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter

}) {

  console.log('filter group: ', headerGroups);



  return (
    <div className="s20-filter">

      <div className="s20-filter__select-carrier">
        {/* technology select */}
      <SelectFilter
        column={ headerGroups[1].headers[0]}
      />
      </div>

      <div className="s20-filter__price-range">
        {/* mora da se postavi between u headers */}
        <NumberFilter
          column={ headerGroups[1].headers[6]}
        />

      </div>

      <div className="s20-filter__checkbox">
        <span> TV </span>
        <CheckboxFilter
          column={headerGroups[1].headers[4]}
          nonValue={ '/' }
        />

        <span> Hardware </span>
        <CheckboxFilter
          column={headerGroups[1].headers[5]}
          nonValue={ 'hardware' }
        />
      </div>


      <div>
        {/* technology select */}
        <SelectFilter
          column={ headerGroups[1].headers[3]}
        />
      </div>


      <div>
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      </div>

    </div>
  );
}
